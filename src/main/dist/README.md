# Caribou HD Performance Testing
## Configuring and Running
1. Replace `Client_Truststore` with your Dispatch's `Client_Truststore`.

2. Set configuration options in `perf-testing.yml`. At minimum, specify Dispatch's hostname and the path to Dispatch's
trust store from step 1 (if different from `Client_Truststore`).

3. Run your desired performance test:

    ```
    ./bin/caribou-hd-perf-testing -c perf-testing.yml -d <NUM_DATA_SOURCES> -o <OBJECTS_PER_DATA_SOURCE> -u <USERNAME> -p <PASSWORD>
    ```

    Which will run a test to hard delete `<NUM_DATA_SOURCES>` data sources with `<OBJECTS_PER_DATA_SOURCE>` objects
    each, under the given user. An encrypted password can be specified instead, using the option `-e` instead of `-p`.
    If an encrypted password is used, then `secureServerPassword` must be set in the config file.

For more usage information, run `./bin/caribou-hd-perf-testing`.

## Logging
Logs will be written to `log/perf-testing.log`. To configure logging, modify `conf/log4j.properties`.
