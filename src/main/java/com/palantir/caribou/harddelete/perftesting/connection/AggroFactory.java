/*
 * Copyright 2015 Palantir Technologies, Inc. All rights reserved.
 */

package com.palantir.caribou.harddelete.perftesting.connection;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.Properties;

import javax.security.auth.login.FailedLoginException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.palantir.model.session.UserSessionToken;
import com.palantir.service.impl.login.SimplePasswordLoginRequest;
import com.palantir.service.prefs.AggroPrefs;
import com.palantir.service.prefs.PropertyProviderBackedAggroPrefs;
import com.palantir.service.services.AggroModule;
import com.palantir.service.services.AggroModules;

public class AggroFactory {
    private static final Logger LOG = LoggerFactory.getLogger(AggroFactory.class);

    private static final String SERVER_ADDRESS = "SERVER_ADDRESS";
    private static final String SERVER_PORT = "SERVER_PORT";
    private static final String USE_SSL = "USE_SSL";
    private static final String SECURE_SERVER_PASSWORD = "SECURE_SERVER_PASSWORD";
    private static final String URL_TAIL_PREFIX = "URL_TAIL_PREFIX";
    private static final String DEFAULT_URL_TAIL_PREFIX = "/services/services/";
    private static final String DEFAULT_SECURE_SERVER_PASSWORD = "-";

    private final String host;
    private final int port;
    private final boolean useSsl;
    private final String trustStore;

    private AggroModule aggroModule = null;

    @JsonCreator
    public AggroFactory(
            @JsonProperty("host") String host,
            @JsonProperty("port") int port,
            @JsonProperty("useSsl") Boolean useSsl,
            @JsonProperty("trustStore") String trustStore) {
        checkArgument(StringUtils.isNotBlank(host), "Aggro host cannot be blank.");
        checkArgument(port >= 1 && port <= 65535, "Aggro port must be in range 1-65535.");
        checkNotNull(useSsl, "Aggro useSsl cannot be null.");
        checkArgument(StringUtils.isNotBlank(trustStore), "Aggro trustStore cannot be blank.");

        File trustStoreFile = new File(trustStore);
        checkArgument(trustStoreFile.exists(), "Path to trustStore is invalid. Full path: " +
                trustStoreFile.getAbsolutePath());
        System.setProperty("javax.net.ssl.trustStore", trustStore);

        this.host = host;
        this.port = port;
        this.useSsl = useSsl;
        this.trustStore = trustStore;
    }

    @JsonProperty
    public String getHost() {
        return host;
    }

    @JsonProperty
    public int getPort() {
        return port;
    }

    @JsonProperty
    public boolean getUseSsl() {
        return useSsl;
    }

    @JsonProperty
    public String getTrustStore() {
        return trustStore;
    }

    @JsonIgnore
    public UserSessionToken loginUser(String username, String password) throws FailedLoginException {
        LOG.debug("Getting user session token for user {}...", username);
        UserSessionToken user = getAggroModule().getLoginService().loginCreateNewSession(new SimplePasswordLoginRequest(
                username, password));
        if (user == null) {
            throw new FailedLoginException("Error logging in via Aggro as user (username=" + username + "). "
                    + "Please verify credentials and connection details.");
        }
        LOG.debug("Got user session token for user {}.", username);
        return user;
    }

    @JsonIgnore
    public void logoutUser(UserSessionToken user) {
        getAggroModule().getLoginService().logout(user);
    }

    @JsonIgnore
    public AggroModule getAggroModule() {
        if (aggroModule == null) {
            aggroModule = createAggroModule();
        }
        return aggroModule;
    }

    @JsonIgnore
    public AggroModule createAggroModule() {
        return AggroModules.createModule(getAggroPrefs());
    }

    @JsonIgnore
    private AggroPrefs getAggroPrefs() {
        return new PropertyProviderBackedAggroPrefs(new PropertyProviderBackedAggroPrefs.PropertiesProvider() {
            @Override
            public Properties getProperties() throws Exception {
                return getAggroProperties();
            }
        });
    }

    @JsonIgnore
    public Properties getAggroProperties() {
        Properties props = new Properties(PropertyProviderBackedAggroPrefs.DEFAULTS);
        props.setProperty(SERVER_ADDRESS, host);
        props.setProperty(SERVER_PORT, String.valueOf(port));
        props.setProperty(USE_SSL, String.valueOf(useSsl));
        props.setProperty(SECURE_SERVER_PASSWORD, DEFAULT_SECURE_SERVER_PASSWORD);
        props.setProperty(URL_TAIL_PREFIX, DEFAULT_URL_TAIL_PREFIX);
        return props;
    }
}
