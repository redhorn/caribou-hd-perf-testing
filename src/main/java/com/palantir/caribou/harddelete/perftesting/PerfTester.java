package com.palantir.caribou.harddelete.perftesting;

import static org.apache.commons.collections.CollectionUtils.isEqualCollection;
import static org.apache.commons.lang3.time.DurationFormatUtils.formatDurationHMS;

import java.util.List;

import javax.security.auth.login.FailedLoginException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.palantir.api.harddelete.DataSourceIdsDoNotExistException;
import com.palantir.api.harddelete.HardDeleteInfo;
import com.palantir.caribou.harddelete.perftesting.data.DataCreationException;
import com.palantir.caribou.harddelete.perftesting.data.DataCreator;
import com.palantir.caribou.harddelete.perftesting.data.DataDeleter;

public class PerfTester {
    private static final Logger LOG = LoggerFactory.getLogger(PerfTester.class);
    private static final String SEPARATOR = StringUtils.repeat('-', 100);

    private final DataCreator dataCreator;
    private final DataDeleter dataDeleter;

    public PerfTester(DataCreator dataCreator, DataDeleter dataDeleter) {
        this.dataCreator = dataCreator;
        this.dataDeleter = dataDeleter;
    }

    public void runAggroTest(int numDataSources, int numObjectsPerDataSource) {
        LOG.info(SEPARATOR);
        LOG.info("Running Aggro-Based Hard Delete Test");
        LOG.info("Creating then deleting {} data sources with {} objects each ({} total objects)",
                numDataSources, numObjectsPerDataSource, numDataSources * numObjectsPerDataSource);
        LOG.info(SEPARATOR);

        try {
            // Create the test data
            LOG.info("Creating test data ({} data sources, with {} objects per data source - {} total objects)...",
                    numDataSources, numObjectsPerDataSource, numDataSources * numObjectsPerDataSource);
            DataCreator.CreatedIds ids = dataCreator.createDatasourcesBatched(numDataSources, numObjectsPerDataSource);
            LOG.info("Finished creating test data.");

            // Delete the test data
            LOG.info("Performing delete of {} data sources...", numDataSources);
            DataDeleter.Result result = dataDeleter.hardDeleteDataSources(ids.getDataSourceIds());
            LOG.info("Finished performing delete of {} data sources. Time taken: {} ms",
                    numDataSources, result.getTotalTime());

            // Verify that the delete was successful
            verifyAndPrintResults(result.getInfo(), ids.getDataSourceIds(), ids.getObjectIds(), numDataSources,
                    numObjectsPerDataSource, result.getTotalTime());
        }
        catch (FailedLoginException e) {
            LOG.error("TEST FAILED: Error logging in.", e);
        }
        catch (DataCreationException e) {
            LOG.error("TEST FAILED: Error creating test data.", e);
        }
        catch (DataSourceIdsDoNotExistException e) {
            LOG.error("TEST FAILED: Error deleting data sources.", e);
        }
    }

    private void verifyAndPrintResults(HardDeleteInfo info, List<Long> dataSourceIds, List<Long> objectIds,
                                       int numDataSources, int numObjectsPerDataSource, long totalTime)
            throws FailedLoginException {
        LOG.info("Verifying...");
        boolean dataSourcesAsExpected = isEqualCollection(dataSourceIds, info.getDeletedDataSourceIds());
        boolean objectsAsExpected = isEqualCollection(objectIds, info.getAffectedObjectIds());
        isEqualCollection(objectIds, info.getAffectedObjectIds());
        List<Long> remainingDsIds = dataCreator.loadDataSourceIdsBatched(dataSourceIds);
        List<Long> remainingObjIds = dataCreator.loadObjectIdsBatched(objectIds);

        LOG.info(SEPARATOR);
        LOG.info("Test Results:");
        LOG.info(SEPARATOR);
        if (dataSourcesAsExpected && objectsAsExpected && remainingDsIds.isEmpty() && remainingObjIds.isEmpty()) {
            LOG.info("TEST SUCCEEDED: All data sources and objects deletes as expected.");
        }
        if (!dataSourcesAsExpected) {
            LOG.error("TEST FAILED: Hard delete info's deleted data sources were not as expected. " +
                            "Expected: {}. Actual: {}.",
                    dataSourceIds, info.getDeletedDataSourceIds());
        }
        if (!objectsAsExpected) {
            LOG.error("TEST FAILED: Hard delete info's affected objects were not as expected. " +
                            "Expected: {}. Actual: {}.",
                    objectIds, info.getAffectedObjectIds());
        }
        if (!remainingDsIds.isEmpty()) {
            LOG.error("TEST FAILED: The following data sources were not deleted as expected: {}", remainingDsIds);
        }
        if (!remainingObjIds.isEmpty()) {
            LOG.error("TEST FAILED: The following objects were not deleted as expected: {}", remainingObjIds);
        }
        LOG.info("TEST FINISHED: Deleted {} data sources with {} objects each ({} total objects) in {}.",
                numDataSources, numObjectsPerDataSource, numDataSources * numObjectsPerDataSource,
                formatDurationHMS(totalTime));
        LOG.info(SEPARATOR);
    }
}
