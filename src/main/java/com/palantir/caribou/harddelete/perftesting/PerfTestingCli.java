package com.palantir.caribou.harddelete.perftesting;

import static java.lang.Integer.parseInt;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.palantir.caribou.harddelete.perftesting.connection.PasswordManager;
import com.palantir.caribou.harddelete.perftesting.data.DataCreator;
import com.palantir.caribou.harddelete.perftesting.data.DataDeleter;

public class PerfTestingCli {
    public static final String CLI_NAME = "caribou-hd-perf-testing";
    private static final String CONFIG_FILE_OPT = "c";
    private static final String USERNAME_OPT = "u";
    private static final String PASSWORD_OPT = "p";
    private static final String ENC_PASSWORD_OPT = "e";
    private static final String NUM_DATA_SOURCES_OPT = "d";
    private static final String NUM_OBJECTS_OPT = "o";

    public static void main(String[] args) {
        Options options = getOptions();
        HelpFormatter help = new HelpFormatter();

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);

            // Get and parse the config file
            PerfTestingConfiguration config = new ObjectMapper(new YAMLFactory()).readValue(
                    new File(line.getOptionValue(CONFIG_FILE_OPT)),
                    PerfTestingConfiguration.class);

            // Get command line options
            String username = line.getOptionValue(USERNAME_OPT);
            String password = getPassword(line, config.getSecureServerPassword());
            int numDataSources = parseInt(line.getOptionValue(NUM_DATA_SOURCES_OPT));
            int numObjectsPerDataSource = parseInt(line.getOptionValue(NUM_OBJECTS_OPT));

            // Run the test
            PerfTester tester = new PerfTester(
                    new DataCreator(
                            config.getAggroFactory(),
                            config.getRealmId(),
                            config.getAclId(),
                            config.getObjectTypeUri(),
                            config.getPropertyTypeUri(),
                            username,
                            password,
                            config.getDataSourceBatchSize(),
                            config.getObjectBatchSize(),
                            config.getDsNamePrefix()),
                    new DataDeleter(
                            config.getPalantirClientFactory(),
                            config.getRealmId(),
                            username,
                            password,
                            config.getDeleteBatchSize(),
                            config.getInvestigationBehaviour(),
                            config.getGraphBehaviour()));
            tester.runAggroTest(numDataSources, numObjectsPerDataSource);
        }
        catch (ParseException e) {
            System.out.println(e.getMessage());
            help.printHelp(CLI_NAME, options, true);
        }
        catch (IOException e) {
            System.out.println("Error reading config file.");
            e.printStackTrace(System.out);
            help.printHelp(CLI_NAME, options, true);
        }
        catch (GeneralSecurityException e) {
            System.out.println("Error decrypting password.");
            e.printStackTrace(System.out);
        }
        finally {
            System.exit(0);
        }
    }

    private static String getPassword(CommandLine line, String secureServerPassword) throws GeneralSecurityException {
        if (line.hasOption(PASSWORD_OPT)) {
            return line.getOptionValue(PASSWORD_OPT);
        }
        else if (line.hasOption(ENC_PASSWORD_OPT)) {
            checkArgument(isNotBlank(secureServerPassword),
                    "Secure server password must be specified to use an encrypted password.");
            PasswordManager manager = new PasswordManager(secureServerPassword);
            return manager.decryptPassword(line.getOptionValue(ENC_PASSWORD_OPT));
        }
        else {
            throw new UnsupportedOperationException("Unknown password option specified.");
        }
    }

    private static Options getOptions() {
        OptionGroup passGroup = new OptionGroup()
                .addOption(Option.builder(PASSWORD_OPT)
                        .argName("password")
                        .longOpt("pass")
                        .hasArg()
                        .desc("Password of the user to use for testing (must be able to create/search objects/data " +
                                "sources and perform hard deletes).")
                        .build())
                .addOption(Option.builder(ENC_PASSWORD_OPT)
                        .argName("encrypted-password")
                        .longOpt("encrypted")
                        .hasArg()
                        .desc("Password of the user to use for testing (must be able to create/search objects/data " +
                                "sources and perform hard deletes). Password has been encrypted using secure server.")
                        .build());
        passGroup.setRequired(true);

        return new Options()
                .addOptionGroup(passGroup)
                .addOption(Option.builder(CONFIG_FILE_OPT)
                        .argName("path")
                        .longOpt("config")
                        .hasArg()
                        .required()
                        .desc("Full path to the configuration YAML file for the perf testing CLI.")
                        .build())
                .addOption(Option.builder(USERNAME_OPT)
                        .argName("username")
                        .longOpt("user")
                        .hasArg()
                        .required()
                        .desc("Username of the user to use for testing (must be able to create/search objects/data " +
                                "sources and perform hard deletes).")
                        .build())
                .addOption(Option.builder(NUM_DATA_SOURCES_OPT)
                        .argName("num-data-sources")
                        .longOpt("data-sources")
                        .hasArg()
                        .required()
                        .desc("Number of data sources to create and subsequently delete as part of the test.")
                        .build())
                .addOption(Option.builder(NUM_OBJECTS_OPT)
                        .argName("num-objects")
                        .longOpt("objects")
                        .hasArg()
                        .required()
                        .desc("Number of objects to create and link to each data source, and then subsequently " +
                                "delete as part of the test.")
                        .build());
    }
}
