/*
 * Copyright 2015 Palantir Technologies, Inc. All rights reserved.
 */

package com.palantir.caribou.harddelete.perftesting.connection;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.security.auth.login.FailedLoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.palantir.api.workspace.PalantirClient;
import com.palantir.api.workspace.PalantirClients;
import com.palantir.services.User;

public class PalantirClientFactory {
    private static final Logger LOG = LoggerFactory.getLogger(PalantirClientFactory.class);

    private String serverAddress;
    private int serverPort;
    private Boolean useSsl;

    public PalantirClientFactory(
            @JsonProperty("serverAddress") String serverAddress,
            @JsonProperty("serverPort") int serverPort,
            @JsonProperty("useSsl") Boolean useSsl) {
        checkArgument(isNotBlank(serverAddress), "Dispatch server address must be specified.");
        checkArgument(serverPort >= 1 && serverPort <= 65535,
                "Dispatch server port must be specified and must be in range 1-65535.");
        checkNotNull(useSsl, "Use SSL must be specified");

        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
        this.useSsl = useSsl;
    }

    @JsonProperty
    public String getServerAddress() {
        return serverAddress;
    }

    @JsonProperty
    public int getServerPort() {
        return serverPort;
    }

    @JsonProperty
    public Boolean getUseSsl() {
        return useSsl;
    }

    @JsonIgnore
    public PalantirClient build(String username, char[] password) throws FailedLoginException {
        LOG.debug("Getting Palantir client (host={}, port={}, useSsl={})...", serverAddress, serverPort, useSsl);
        PalantirClient client = PalantirClients.getHeadlessClient();
        client.setServerName(serverAddress);
        client.setServerPort(serverPort);
        client.setUseSSL(useSsl);

        LOG.debug("Logging in user {} to Palantir client...", username);
        User user = client.login(username, password);
        if (user == null) {
            throw new FailedLoginException("Error logging into headless client as user '" + username + "'. "
                    + "Please verify credentials and connection details.");
        }
        LOG.debug("Logged in user {} to Palantir client.", username);

        LOG.debug("Finished getting Palantir client (host={}, port={}, useSsl={}).", serverAddress, serverPort, useSsl);
        return client;
    }
}
