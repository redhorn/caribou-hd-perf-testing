package com.palantir.caribou.harddelete.perftesting;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import static com.google.common.base.Preconditions.checkArgument;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.palantir.api.harddelete.HardDeleteGraphBehavior;
import com.palantir.api.harddelete.HardDeleteInvestigationBehavior;
import com.palantir.caribou.harddelete.perftesting.connection.AggroFactory;
import com.palantir.caribou.harddelete.perftesting.connection.PalantirClientFactory;

public class PerfTestingConfiguration {
    private final String host;
    private final int port;
    private final boolean useSsl;
    private final String trustStore;
    private final int dataSourceBatchSize;
    private final int objectBatchSize;
    private final long realmId;
    private final long aclId;
    private final String objectTypeUri;
    private final String propertyTypeUri;
    private final int deleteBatchSize;
    private final HardDeleteInvestigationBehavior investigationBehaviour;
    private final HardDeleteGraphBehavior graphBehaviour;
    private final String secureServerPassword;
    private final String dsNamePrefix;

    @JsonCreator
    public PerfTestingConfiguration(
            @JsonProperty("host") String host,
            @JsonProperty("port") Integer port,
            @JsonProperty("useSsl") Boolean useSsl,
            @JsonProperty("trustStore") String trustStore,
            @JsonProperty("dataSourceBatchSize") Integer dataSourceBatchSize,
            @JsonProperty("objectBatchSize") Integer objectBatchSize,
            @JsonProperty("realmId") Long realmId,
            @JsonProperty("aclId") Long aclId,
            @JsonProperty("objectTypeUri") String objectTypeUri,
            @JsonProperty("propertyTypeUri") String propertyTypeUri,
            @JsonProperty("deleteBatchSize") Integer deleteBatchSize,
            @JsonProperty("investigationBehaviour") HardDeleteInvestigationBehavior investigationBehaviour,
            @JsonProperty("graphBehaviour") HardDeleteGraphBehavior graphBehaviour,
            @JsonProperty("secureServerPassword") String secureServerPassword,
            @JsonProperty("dsNamePrefix") String dsNamePrefix) {
        checkArgument(isNotBlank(host), "Dispatch host name must be specified.");
        checkArgument(port == null || (port >= 1 && port <= 65535),
                "If specified, dispatch port must be in range 1-65535.");
        checkArgument(isNotBlank(host), "Path to trust store must be specified.");

        this.host = host;
        this.port = defaultIfNull(port, 3280);
        this.useSsl = defaultIfNull(useSsl, true);
        this.trustStore = trustStore;
        this.dataSourceBatchSize = defaultIfNull(dataSourceBatchSize, 100);
        this.objectBatchSize = defaultIfNull(objectBatchSize, 5);
        this.realmId = defaultIfNull(realmId, 1L);
        this.aclId = defaultIfNull(aclId, 2L);
        this.objectTypeUri = defaultIfBlank(objectTypeUri, "com.palantir.object.Person");
        this.propertyTypeUri = defaultIfBlank(propertyTypeUri, "com.palantir.property.Name");
        this.deleteBatchSize = defaultIfNull(deleteBatchSize, 100);
        this.investigationBehaviour = defaultIfNull(investigationBehaviour, HardDeleteInvestigationBehavior.UNLOCKED);
        this.graphBehaviour = defaultIfNull(graphBehaviour, HardDeleteGraphBehavior.REDACT);
        this.secureServerPassword = secureServerPassword;
        this.dsNamePrefix = defaultIfBlank(secureServerPassword, "hd-perf-test-data-source-");
    }

    @JsonProperty
    public String getHost() {
        return host;
    }

    @JsonProperty
    public int getPort() {
        return port;
    }

    @JsonProperty
    public boolean isUseSsl() {
        return useSsl;
    }

    @JsonProperty
    public String getTrustStore() {
        return trustStore;
    }

    @JsonProperty
    public int getDataSourceBatchSize() {
        return dataSourceBatchSize;
    }

    @JsonProperty
    public int getObjectBatchSize() {
        return objectBatchSize;
    }

    @JsonProperty
    public long getRealmId() {
        return realmId;
    }

    @JsonProperty
    public long getAclId() {
        return aclId;
    }

    @JsonProperty
    public String getObjectTypeUri() {
        return objectTypeUri;
    }

    @JsonProperty
    public String getPropertyTypeUri() {
        return propertyTypeUri;
    }

    @JsonProperty
    public int getDeleteBatchSize() {
        return deleteBatchSize;
    }

    @JsonProperty
    public HardDeleteInvestigationBehavior getInvestigationBehaviour() {
        return investigationBehaviour;
    }

    @JsonProperty
    public HardDeleteGraphBehavior getGraphBehaviour() {
        return graphBehaviour;
    }

    @JsonProperty
    public String getSecureServerPassword() {
        return secureServerPassword;
    }

    @JsonProperty
    public String getDsNamePrefix() {
        return dsNamePrefix;
    }

    @JsonIgnore
    public PalantirClientFactory getPalantirClientFactory() {
        return new PalantirClientFactory(getHost(), getPort(), isUseSsl());
    }

    @JsonIgnore
    public AggroFactory getAggroFactory() {
        return new AggroFactory(getHost(), getPort(), isUseSsl(), getTrustStore());
    }
}
