/*
 * Copyright 2015 Palantir Technologies, Inc. All rights reserved.
 */

package com.palantir.caribou.harddelete.perftesting.connection;

import static com.google.common.base.Preconditions.checkNotNull;

import java.security.GeneralSecurityException;

import com.palantir.secureserver.SecureServer;
import com.palantir.util.server.PTSecurityException;

public class PasswordManager {
    private final char[] secureServerPassword;

    public PasswordManager(String secureServerPassword) {
        this(checkNotNull(secureServerPassword, "Secure server password cannot be null.").toCharArray());
    }

    public PasswordManager(char[] secureServerPassword) {
        this.secureServerPassword = checkNotNull(secureServerPassword, "Secure server password cannot be null.");
    }

    public String decryptPassword(String encrypted) throws GeneralSecurityException {
        try {
            return SecureServer.decryptPassword(secureServerPassword, encrypted);
        }
        catch (GeneralSecurityException | PTSecurityException e) {
            throw new GeneralSecurityException(
                    "Error decrypting password. Please verify that credentials and configuration are correct.", e);
        }
    }

    public String encryptPassword(String encrypted) throws GeneralSecurityException {
        try {
            return SecureServer.encryptPassword(secureServerPassword, encrypted);
        }
        catch (GeneralSecurityException | PTSecurityException e) {
            throw new GeneralSecurityException(
                    "Error encrypting password. Please verify that credentials and configuration are correct.", e);
        }
    }
}
