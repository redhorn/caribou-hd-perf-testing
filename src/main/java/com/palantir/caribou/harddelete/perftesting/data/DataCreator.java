package com.palantir.caribou.harddelete.perftesting.data;

import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import static com.palantir.model.impl.builders.PropertyValueBuilders.newSimpleValue;
import static com.palantir.service.impl.object.write.builder.ObjectWriteBuilders.newCreateObject;
import static com.palantir.service.impl.object.write.builder.ObjectWriteBuilders.newCreateProperty;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.FailedLoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.palantir.caribou.harddelete.perftesting.connection.AggroFactory;
import com.palantir.model.Role;
import com.palantir.model.datasource.DataSource;
import com.palantir.model.datasource.DataSourceRecord;
import com.palantir.model.impl.datasource.SimpleDataSourceRecord;
import com.palantir.model.object.PalantirObject;
import com.palantir.model.session.UserSessionToken;
import com.palantir.service.impl.datasource.SimpleCreateDataSource;
import com.palantir.service.impl.object.include.builders.ObjectLoadCriteriaBuilders;
import com.palantir.service.object.write.CreateObject;
import com.palantir.service.object.write.CreateObjectsResult;
import com.palantir.service.services.AggroModule;

public class DataCreator {
    private static final Logger LOG = LoggerFactory.getLogger(DataCreator.class);
    private final AggroFactory aggroFactory;
    private final long realmId;
    private final long aclId;
    private final String objectTypeUri;
    private final String propertyTypeUri;
    private final String username;
    private final String password;
    private final int dataSourceBatchSize;
    private final int objectBatchSize;
    private final String dsNamePrefix;

    public DataCreator(AggroFactory aggroFactory, long realmId, long aclId, String objectTypeUri,
                       String propertyTypeUri, String username, String password, int dataSourceBatchSize,
                       int objectBatchSize, String dsNamePrefix) {
        this.aggroFactory = aggroFactory;
        this.realmId = realmId;
        this.aclId = aclId;
        this.objectTypeUri = objectTypeUri;
        this.propertyTypeUri = propertyTypeUri;
        this.username = username;
        this.password = password;
        this.dataSourceBatchSize = dataSourceBatchSize;
        this.objectBatchSize = objectBatchSize;
        this.dsNamePrefix = dsNamePrefix;
    }

    public CreatedIds createDatasourcesBatched(int numDataSources, int numObjectsPerDataSource)
            throws FailedLoginException, DataCreationException {
        AggroModule aggro = aggroFactory.getAggroModule();
        UserSessionToken user = null;

        try {
            user = aggroFactory.loginUser(username, password);
            List<Long> dataSourceIds = createDatasourcesBatched(aggro, user, realmId, aclId, numDataSources,
                    dataSourceBatchSize);
            List<Long> objectIds = createObjectsPerDataSourceBatched(aggro, user, realmId, aclId, objectTypeUri,
                    propertyTypeUri, dataSourceIds, numObjectsPerDataSource, objectBatchSize);
            return new CreatedIds(dataSourceIds, objectIds);
        }
        finally {
            aggroFactory.logoutUser(user);
        }
    }

    private List<Long> createDatasources(AggroModule aggro, UserSessionToken user, long realmId, long aclId,
                                                int numDatasources) throws DataCreationException {
        List<Long> ids = Lists.newArrayList();
        LOG.debug("Creating {} data sources...", numDatasources);
        for (int i = 0; i < numDatasources; i++) {
            ids.add(aggro.getDataSourceService().createDataSource(user, realmId, new SimpleCreateDataSource(
                    aclId,
                    dsNamePrefix + i,
                    dsNamePrefix + i,
                    realmId
            )));
        }

        LOG.debug("Verifying that creation was successful...");
        if (!isEqualCollection(loadDataSourceIds(aggro, user, realmId, ids), ids)) {
            throw new DataCreationException("Failed to load created data sources. The following data sources IDs " +
                    "were expected but not found: " + ids);
        }

        LOG.debug("Created {} data sources: {}", numDatasources, ids);
        return ids;
    }

    private List<Long> createDatasourcesBatched(AggroModule aggro, UserSessionToken user, long realmId,
                                                       long aclId, int numDatasources, int batchSize)
            throws DataCreationException {
        int fullBatches = numDatasources / batchSize;
        int remainder = numDatasources % batchSize;
        int totalBatches = fullBatches + ((remainder > 0) ? 1 : 0);
        LOG.debug("Creating {} data sources in {} batches...", numDatasources, totalBatches);

        List<Long> ids = Lists.newArrayList();
        for (int batch = 0; batch < fullBatches; batch++) {
            LOG.debug("Creating batch {} of {}...", batch + 1, totalBatches);
            ids.addAll(createDatasources(aggro, user, realmId, aclId, batchSize));
            LOG.debug("Finished creating batch {} of {}.", batch + 1, totalBatches);
        }

        if (remainder > 0) {
            LOG.debug("Creating batch {} of {}...", totalBatches, totalBatches);
            ids.addAll(createDatasources(aggro, user, realmId, aclId, remainder));
            LOG.debug("Finished creating batch {} of {}.", totalBatches, totalBatches);
        }

        LOG.debug("Finished creating {} data sources in {} batches. IDs: {}", numDatasources, totalBatches, ids);
        return ids;
    }

    private List<Long> createObjectsPerDataSource(AggroModule aggro, UserSessionToken user, long realmId,
                                                        long aclId, String objectTypeUri, String propertyTypeUri,
                                                        List<Long> dataSourceIds, int numObjectsPerDataSource)
            throws DataCreationException {
        List<CreateObject> createObjects = Lists.newArrayList();
        LOG.debug("Creating {} objects per data source for {} data sources...",
                numObjectsPerDataSource, dataSourceIds.size());
        for (long dataSourceId : dataSourceIds) {
            for (int i = 0; i < numObjectsPerDataSource; i++) {
                createObjects.add(newCreateObject()
                        .objectTypeUri(objectTypeUri)
                        .addInitialProperty(newCreateProperty()
                                        .type(propertyTypeUri)
                                        .value(newSimpleValue("test object " + i + " for datasource " + dataSourceId))
                                        .role(Role.NONE)
                                        .dataSourceRecords(Collections.<DataSourceRecord>singleton(
                                                new SimpleDataSourceRecord(dataSourceId, aclId)))
                                        .build()
                        )
                        .build());
            }
        }

        CreateObjectsResult results = aggro.getObjectWriteService().createObjects(user, realmId, createObjects);
        if (results == null) {
            throw new DataCreationException("Error creating objects. Create result was null.");
        }
        else if (!results.getFailures().isEmpty()) {
            throw new DataCreationException("Error creating objects. Failures: " + results.getFailures());
        }

        List<Long> ids = results.getCreatedObjectIds();
        LOG.debug("Verifying that creation was successful...");
        if (!isEqualCollection(loadObjectIds(aggro, user, realmId, ids), ids)) {
            throw new DataCreationException("Failed to load created objects. The following object IDs were expected " +
                    "but not found: " + ids);
        }

        LOG.debug("Created {} objects per data source for {} data sources: {}",
                numObjectsPerDataSource, dataSourceIds.size(), ids);
        return ids;
    }

    private List<Long> createObjectsPerDataSourceBatched(AggroModule aggro, UserSessionToken user, long realmId,
                                                                long aclId, String objectTypeUri,
                                                                String propertyTypeUri, List<Long> datasourceIds,
                                                                int numObjectsPerDataSource, int batchSize)
            throws DataCreationException {
        List<List<Long>> batches = Lists.partition(datasourceIds, batchSize);
        int count = 1;
        List<Long> ids = Lists.newArrayList();
        LOG.debug("Creating {} objects per data source for {} data sources in {} batches...",
                numObjectsPerDataSource, datasourceIds.size(), batches.size());
        for (List<Long> batch : batches) {
            LOG.debug("Creating batch {} of {}...", count, batches.size());
            ids.addAll(createObjectsPerDataSource(aggro, user, realmId, aclId, objectTypeUri, propertyTypeUri, batch,
                    numObjectsPerDataSource));
            LOG.debug("Finished creating batch {} of {}.", count++, batches.size());
        }
        LOG.debug("Creating {} objects per data source for {} data sources in {} batches. IDs: {}",
                numObjectsPerDataSource, datasourceIds.size(), batches.size(), ids);
        return ids;
    }

    public List<Long> loadDataSourceIdsBatched(List<Long> dataSourceIds) throws FailedLoginException {
        AggroModule aggro = aggroFactory.getAggroModule();
        UserSessionToken user = null;

        try {
            user = aggroFactory.loginUser(username, password);
            return loadDataSourceIdsBatched(aggro, user, realmId, dataSourceIds, dataSourceBatchSize);
        }
        finally {
            aggroFactory.logoutUser(user);
        }
    }

    private List<Long> loadDataSourceIds(AggroModule aggro, UserSessionToken user, long realmId,
                                               List<Long> datasourceIds) {
        List<Long> ids = Lists.newArrayList();
        LOG.debug("Loading data source IDs: {}", datasourceIds);
        for (DataSource dataSource : aggro.getDataSourceService().loadDataSources(user, realmId, datasourceIds)) {
            ids.add(dataSource.getId());
        }
        LOG.debug("Finished loading data source IDs: {}", ids);
        return ids;
    }

    private List<Long> loadDataSourceIdsBatched(AggroModule aggro, UserSessionToken user, long realmId,
                                                       List<Long> datasourceIds, int batchSize) {
        List<Long> results = Lists.newArrayList();
        List<List<Long>> batches = Lists.partition(datasourceIds, batchSize);
        int count = 1;
        LOG.debug("Loading {} data sources split into {} batches...", datasourceIds.size(), batches.size());
        for (List<Long> batch : batches) {
            LOG.debug("Loading batch {} of {}...", count, batches.size());
            results.addAll(loadDataSourceIds(aggro, user, realmId, batch));
            LOG.debug("Finished loading batch {} of {}.", count++, batches.size());
        }
        LOG.debug("Finished loading {} data sources split into {} batches. Results: {}",
                datasourceIds.size(), batches.size(), results);
        return results;
    }

    public List<Long> loadObjectIdsBatched(List<Long> objectIds) throws FailedLoginException {
        AggroModule aggro = aggroFactory.getAggroModule();
        UserSessionToken user = null;

        try {
            user = aggroFactory.loginUser(username, password);
            return loadObjectIdsBatched(aggro, user, realmId, objectIds, dataSourceBatchSize);
        }
        finally {
            aggroFactory.logoutUser(user);
        }
    }

    private List<Long> loadObjectIds(AggroModule aggro, UserSessionToken user, long realmId,
                                           List<Long> objectIds) {
        List<Long> ids = Lists.newArrayList();
        for (Map.Entry<Long, PalantirObject> entry : aggro.getObjectLoadService()
                .loadObjects(user, realmId, objectIds, ObjectLoadCriteriaBuilders.newBuilder().build())
                .getObjectsByRequestedIds().entrySet()) {
            ids.add(entry.getValue().getId());
        }
        return ids;
    }

    private List<Long> loadObjectIdsBatched(AggroModule aggro, UserSessionToken user, long realmId,
                                                  List<Long> objectIds, int batchSize) {
        List<Long> results = Lists.newArrayList();
        List<List<Long>> batches = Lists.partition(objectIds, batchSize);
        int count = 1;
        LOG.debug("Loading {} objects split into {} batches...", objectIds.size(), batches.size());
        for (List<Long> batch : batches) {
            LOG.debug("Loading batch {} of {}...", count, batches.size());
            results.addAll(loadObjectIds(aggro, user, realmId, batch));
            LOG.debug("Finished loading batch {} of {}.", count++, batches.size());
        }
        LOG.debug("Finished loading {} objects split into {} batches. Results: {}",
                objectIds.size(), batches.size(), results);
        return results;
    }

    public static class CreatedIds {
        private final List<Long> dataSourceIds;
        private final List<Long> objectIds;

        private CreatedIds(List<Long> dataSourceIds, List<Long> objectIds) {
            this.dataSourceIds = dataSourceIds;
            this.objectIds = objectIds;
        }

        public List<Long> getDataSourceIds() {
            return dataSourceIds;
        }

        public List<Long> getObjectIds() {
            return objectIds;
        }
    }
}
