package com.palantir.caribou.harddelete.perftesting.data;

public class DataCreationException extends Exception {
    public DataCreationException() {
        super();
    }

    public DataCreationException(String message) {
        super(message);
    }

    public DataCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataCreationException(Throwable cause) {
        super(cause);
    }
}
