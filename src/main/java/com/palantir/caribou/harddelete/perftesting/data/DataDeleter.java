package com.palantir.caribou.harddelete.perftesting.data;

import java.util.Collection;
import java.util.List;

import javax.security.auth.login.FailedLoginException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.palantir.api.harddelete.DataSourceIdsDoNotExistException;
import com.palantir.api.harddelete.HardDeleteGraphBehavior;
import com.palantir.api.harddelete.HardDeleteInfo;
import com.palantir.api.harddelete.HardDeleteInvestigationBehavior;
import com.palantir.api.workspace.PalantirClient;
import com.palantir.api.workspace.PalantirConnection;
import com.palantir.api.workspace.PalantirHeadlessClientContext;
import com.palantir.caribou.harddelete.perftesting.connection.PalantirClientFactory;
import com.palantir.services.ptobject.harddelete.HardDeleteInfoImpl;

public class DataDeleter {
    private static final Logger LOG = LoggerFactory.getLogger(DataDeleter.class);

    private final PalantirClientFactory clientFactory;
    private final long realmId;
    private final String username;
    private final String password;
    private final int batchSize;
    private final HardDeleteInvestigationBehavior investigationBehaviour;
    private final HardDeleteGraphBehavior graphBehaviour;

    public DataDeleter(PalantirClientFactory clientFactory, long realmId, String username, String password,
                       int batchSize, HardDeleteInvestigationBehavior investigationBehaviour,
                       HardDeleteGraphBehavior graphBehaviour) {
        this.clientFactory = clientFactory;
        this.realmId = realmId;
        this.username = username;
        this.password = password;
        this.batchSize = batchSize;
        this.investigationBehaviour = investigationBehaviour;
        this.graphBehaviour = graphBehaviour;
    }

    public Result hardDeleteDataSources(List<Long> dataSourceIds) throws FailedLoginException,
            DataSourceIdsDoNotExistException {
        PalantirClient client = null;

        try {
            LOG.debug("Connecting to Palantir via headless client...");
            client = clientFactory.build(username, password.toCharArray());
            PalantirHeadlessClientContext context = client.getClientContext();
            PalantirConnection connection = context.getPalantirConnection(realmId);
            LOG.debug("Connected to Palantir.");

            LOG.debug("Executing batched deletion by datasource ID from Palantir of {} data sources...",
                    dataSourceIds.size());

            List<List<Long>> batches = Lists.partition(dataSourceIds, batchSize);
            int count = 1;
            long start = System.nanoTime();
            HardDeleteInfo combinedInfo = new HardDeleteInfoImpl();
            for (List<Long> batch : batches) {
                LOG.debug("Starting batch {} of {}...", count, batches.size());
                addAllToHardDeleteInfo(combinedInfo,
                        executeDeleteByDatasourceIdFromPalantir(connection, Lists.newArrayList(batch)));
                LOG.debug("Finished batch {} of {}.", count++, batches.size());
            }
            long end = System.nanoTime();
            long totalTime = (end - start) / 1000000;

            logDeleteInfo("Result of Batched Hard Deletion by Data Source ID:", combinedInfo);

            LOG.debug("Executed batched deletion by datasource ID from Palantir of {} data sources. (Took {} ms.)",
                    dataSourceIds.size(), totalTime);
            return new Result(combinedInfo, totalTime);
        }
        finally {
            if (client != null) {
                client.shutdown();
            }
        }
    }

    private HardDeleteInfo executeDeleteByDatasourceIdFromPalantir(PalantirConnection connection,
                                                                   Collection<Long> datasourceIds)
            throws DataSourceIdsDoNotExistException {
        LOG.debug("Executing hard delete by data source ID from Palantir of {} data sources...", datasourceIds.size());
        long start = System.nanoTime();
        HardDeleteInfo info = connection.hardDeleteDataSources(
                datasourceIds,
                investigationBehaviour,
                graphBehaviour);
        long end = System.nanoTime();
        LOG.debug("Executed hard delete by data source ID from Palantir of {} data sources. (Took {} ms.)",
                datasourceIds.size(), (end - start) / 1000000);

        return info;
    }

    private void addAllToHardDeleteInfo(HardDeleteInfo info, HardDeleteInfo toAdd) {
        if (toAdd.getAffectedInvestigationIds() != null)
            info.getAffectedInvestigationIds().addAll(toAdd.getAffectedInvestigationIds());
        if (toAdd.getAffectedPersistentSearchIds() != null)
            info.getAffectedPersistentSearchIds().addAll(toAdd.getAffectedPersistentSearchIds());
        if (toAdd.getAffectedRealmIds() != null)
            info.getAffectedRealmIds().addAll(toAdd.getAffectedRealmIds());
        if (toAdd.getAffectedSharedGraphIds() != null)
            info.getAffectedSharedGraphIds().addAll(toAdd.getAffectedSharedGraphIds());
        if (toAdd.getDeletedDataSourceIds() != null)
            info.getDeletedDataSourceIds().addAll(toAdd.getDeletedDataSourceIds());
        if (toAdd.getDeletedObjectIds() != null)
            info.getDeletedObjectIds().addAll(toAdd.getDeletedObjectIds());
        if (toAdd.getInvRealmResolutionsToBreak() != null)
            info.getInvRealmResolutionsToBreak().addAll(toAdd.getInvRealmResolutionsToBreak());
        if (toAdd.getLinkedObjectIds() != null)
            info.getLinkedObjectIds().addAll(toAdd.getLinkedObjectIds());
        if (toAdd.getModifiedObjectIds() != null)
            info.getModifiedObjectIds().addAll(toAdd.getModifiedObjectIds());
        if (toAdd.getPendingConflictIds() != null)
            info.getPendingConflictIds().addAll(toAdd.getPendingConflictIds());
    }

    private void logDeleteInfo(String title, HardDeleteInfo deleteInfo) {
        String separator = StringUtils.repeat('-', title.length());
        LOG.debug(separator);
        LOG.debug(title);
        LOG.debug(separator);
        LOG.debug("\tDeleted Data Source IDs: {}", deleteInfo.getDeletedDataSourceIds());
        LOG.debug("\tDeleted Object IDs: {}", deleteInfo.getDeletedObjectIds());
        LOG.debug("\tLinked Object IDs: {}", deleteInfo.getLinkedObjectIds());
        LOG.debug("\tModified Object IDs: {}", deleteInfo.getModifiedObjectIds());
        LOG.debug("\tInv. Realm Resolutions to Break: {}", deleteInfo.getInvRealmResolutionsToBreak());
        LOG.debug("\tPending Conflict IDs: {}", deleteInfo.getPendingConflictIds());
        LOG.debug("\tAffected Investigation IDs: {}", deleteInfo.getAffectedInvestigationIds());
        LOG.debug("\tAffected Realm IDs: {}", deleteInfo.getAffectedRealmIds());
        LOG.debug("\tAffected Object IDs: {}", deleteInfo.getAffectedObjectIds());
        LOG.debug("\tAffected Persistent Search IDs: {}", deleteInfo.getAffectedPersistentSearchIds());
        LOG.debug("\tAffected Shared Graph IDs: {}", deleteInfo.getAffectedSharedGraphIds());
        LOG.debug(separator);
    }

    public static class Result {
        private final HardDeleteInfo info;
        private final long totalTime;

        private Result(HardDeleteInfo info, long totalTime) {
            this.info = info;
            this.totalTime = totalTime;
        }

        public HardDeleteInfo getInfo() {
            return info;
        }

        public long getTotalTime() {
            return totalTime;
        }
    }
}
